# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Capas ###
* Capa de acceso a datos (modelos) User.php y ValuesMatrix.php
* Capa de lógica de negocio Controlador principal HomeController.php y dos Traits Matrix.php - Way.php
  Matrix.php se encarga de crear la matriz principal de NxN en donde se definen los valores de tiempo para el desplazaiento entre ciudades,
  también se encarga de construir las ciudades y el grafo que contiene las rutas y valores de las ciudades
  Way.php contiene la lógica que encuentra el camino más corto de los gatos
  
### Pruebas ###
* MatrixTest.php se encarga de realizar una petición POST al servicio y evalua la respuesta
* LoginTest.php se encarga de realizar la prueba integral desde login hasta la carga en la vista de los resultados

### Install? ###

* Clone repositorio in your server
* Run composer update and then run composer install
* If you prefer create a database or use one you already have in Mysql and Postgres
* Configure your databases in app/databases: Redis, Mysql and Postgres also .env
* Run migrates with "php artisan migrate"
