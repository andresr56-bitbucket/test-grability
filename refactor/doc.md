# Comentarios refactorización de código #

* Como buena práctica se recomienda el uso de lowerCamelCase para el nombre de los métodos o funciones se cambia el nombre por postConfirm
* Se elimina código comentando, para esto se maneja el control de versiones
* Validar el tipo de request (get)
* Validar que service_id y driver_id no estén vacios
* Setear variables $id y $driverId con el valor recibido por get, especificamente driver_id se usa para actualizar y buscar en la DB pero no se tenia validación de este campo.
* Como buena práctica las validaciones en los condicionales se deben usar comparando el dato y el tipo de dato con === o !==, se cambia esto para todos los condicionales
* Se valida que la variable servicio sea tipo objeto
* Siempre esta devolviendo un error, se modifica para que retorne result en caso de que todo este bien
* Como buena practica no se debe sobreescribir una variable, en este caso la variable servicio esta sobre escrita 2 veces
* Se están haciendo dos peticiones a la DB para actualizar Service, se unifica para realizar una sola acción
* No se está validando si las actualizaciones y busquedas en la DB son exitosas, se agrega validación para todas las consultas
* Por buenas prácticas no se debe tener código quemado, para el caso del pushMessage se debe obtener por parametros que vienen de un archivo de configuración
* Por buenas prácticas todas las definiciones de funciones, nombres de variables etc. deben estar en ingles, se realiza cambio.
* Se definenen variables y se usan métodos antes de las validaciones usando recursos innecesarios ($push = Push:make y $pushMessage), se corrige y se usan cuando realmente son necesarios
* Se recomiendo el uso de comillas sencillas no dobles la principal razón es que siempre que se usan comillas dobles, PHP analiza  el string en búsqueda de variables que deban ser interpretadas, se cambia a sencillas dentro  en la llave 'available'
* Se agreron funciones privadas en donde se validan cada unos de los enventos de busqueda y actualización en la DB para tener un mejor control, de esta manera determinar si se hizo o no realmente una acción

