<?php

public function postConfirm(Request $request) {
    if (!$request->isMethod('get')) {
        Response::json(array('error' => '0'));
    }
    $validator = Validator::make($request->all(), [
                'service_id' => 'required|integer',
                'driver_id' => 'required|integer',
    ]);
    if ($validator->fails()) {
        Response::json(array('error' => $validator->errors()->all()));
    }
    $id = Input::get('service_id');
    $driverId = Input::get('driver_id');
    $service = Service::find($id);
    if ($service !== NULL && is_object($service)) {
        if ($service->status_id === '6') {
            return Response::json(array('error' => '2'));
        }
        if ($service->driver_id === NULL && $service->status_id === '1') {
            $findService = $this->validations($id, $driverId);
            if ($findService->user->uuid === '' || $findService === FALSE) {
                return Response::json(array('error' => '0'));
            }
            $push = $this->push($findService);
            if ($push === FALSE){
                return Response::json(array('error' => '0'));
            }
            return Response::json(array('result' => $push)); 
        } else {
            return Response::json(array('error' => '1'));
        }
    } else {
        return Response::json(array('error' => '3'));
    }
}

private function validations($id, $driverId) {
    $updateDriver = Driver::update($driverId, array(
                'available' => '0'
    ));
    $return = FALSE;
    if ($updateDriver !== NULL) {
        $driverTmp = Driver::find($driverId);
        if ($driverTmp !== NULL && is_object($driverTmp)) {
            $updateService = Service::update($id, array(
                        'driver_id' => $driverId,
                        'status_id' => '2',
                        'car_id' => $driverTmp->car_id
            ));
            if ($updateService === TRUE) {
                $findService = Service::find($id);
                if ($findService !== NULL) {
                    $return = $findService;
                }
            }
        }
    }
    return $return;
}

private function push($f) {
    $push = Push::make();
    $pushMessage = 'Tu servicio ha sido confirmado!';
    $result = FALSE;
    switch ($f->user->type) {
        case '1':
            $result = $push->ios($f->user->uuid, $pushMessage, 1, 'honk.wav', 'Open', array('serviceId' => $f->id));
            break;
        default:
            $result = $push->android2($f->user->uuid, $pushMessage, 1, 'default', 'Open', array('serviceId' => $f->id));
            break;
    }
    return $result;
}
