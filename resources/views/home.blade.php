@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Start</div>
                <div class="panel-body">
                    <div class="alert alert-danger" role="alert" style="display: none">
                        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                        <span class="sr-only">Error:</span>
                    </div>
                    <form class="navbar-form navbar-left" method="POST" action="matrix" role="search" id="start_limit">
                        {{csrf_field()}}
                        <label>Include value for the Shopping Center:</label>
                        <div class="form-group">
                            <input type="number" name="shoppings" id="shoppings" value="{{old('limit')}}" class="form-control" placeholder="Search">
                        </div>
                        <button type="submit" class="btn btn-danger" name="sendMatrix">
                            <span class="glyphicon glyphicon-plane" aria-hidden="true"></span> Send
                        </button>
                    </form>
                    <div class="navbar-form">
                        <div class="form-group">
                            <label class="btn btn-default active">
                                <input type="checkbox"  id="myStateButton" name="myStateButton" autocomplete="off" >You want to show the graph?
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">Waiting for services back-end</div>
                <div class="panel-body">
                    <div id="contenedor">
                        <div class="reloj" id="Horas">00</div>
                        <div class="reloj" id="Minutos">:00</div>
                        <div class="reloj" id="Segundos">:00</div>
                        <div class="reloj" id="Centesimas">:00</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="panel panel-warning" id="container_roads_graph" style="display:none">
            <div class="panel-heading" id="heading-tbl-roads_graph">Graph Road more short</div>
            <div class="jumbotron" id="loadingBar">
                <h3 class="display-3">Building the graph in front-end</h3>
                <div class="progress">
                    <div id="bar" class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="0"
                         aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                        <div id="text">0%</div>
                    </div>
                </div>
            </div>
            <div id="mynetworkdos"></div>
        </div>
        <div class="panel panel-primary" id="container_roads" style="display:none">
            <div class="panel-heading" id="heading-tbl-roads">Road more short</div>
            <div class="table-responsive">
                <div class="table-responsive">
                    <!-- Table -->
                    <table class="table" id="roadsTable">
                        <thead>
                        <td><span class="glyphicon glyphicon-plane" aria-hidden="true"></span> Cat
                        </td>
                        <td><span class="glyphicon glyphicon-road" aria-hidden="true"></span> Road</td>
                        <td><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Total time</td>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="panel panel-primary" id="container_matrix" style="display:none">
            <!-- Default panel contents -->
            <div class="panel-heading" id="heading-tbl-matrix">Dashboard of shopping centers</div>
            <div class="table-responsive">
                <!-- Table -->
                <table class="table" id="matrixTable">
                    <thead>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
        
    </div>
</div>
@endsection
