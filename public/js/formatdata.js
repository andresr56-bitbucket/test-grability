function FORMATdata() {
    this.data = '';
}

FORMATdata.prototype.buildData = function (data) {
    response = data;
    this.buildMatrix(data.matrix, data.shoppings);
    this.buildRoads(data.roads);
    if (ACTIVEGRAPH)
        buildGraph(nodes, edges, this);
}

FORMATdata.prototype.buildMatrix = function (matrices, shoppings) {
    let tableHead = $('#matrixTable').find('thead');
    let tableBody = $('#matrixTable').find('tbody');
    let tr = $('<tr></tr>').appendTo(tableHead);
    let td = $('<td><span class="glyphicon glyphicon-plane" aria-hidden="true"></span></td>').appendTo(tr);
    nodes = []
    $.each(shoppings, function (c, shopping) {
        let val = 20;
        if (c === 0 || c === (shoppings.length - 1))
            val = 25;
        td = $('<td>' + shopping.name + '</td>').appendTo(tr);
        nodes[c] = {id: shopping.name, value: val, label: shopping.name, group: c, "color": {
                "border": "rgb(67,67,229)",
                "highlight": {
                    "background": "rgb(67,67,229)",
                    "border": "rgb(67,67,229)"
                },
                "hover": {
                    "background": "rgb(67,67,229)",
                    "border": "rgb(67,67,229)"
                }
            },
            "size": 12.666666984558105, }
    });
    $.each(matrices, function (m, matrix) {
        let tr = $('<tr></tr>').appendTo(tableBody);
        $('<td>C' + (m) + '</td>').appendTo(tr);
        $.each(matrix, function (i, mat) {
            $('<td>' + mat + '</td>').appendTo(tr);
        });
    });
    $("#heading-tbl-matrix").html("Dashboard of shopping centers");
}

FORMATdata.prototype.buildRoads = function (roads) {
    edges = [];
    let j = 0;
    $.each(roads, function (i, item) {
        let title = item.name;
        $('#roadsTable').append('<tr><td>' + item.name + '</td><td>' + item.road + '</td><td>' + item.road[item.road.length - 1] + '</td></tr>');
        let color = "#" + ((1 << 24) * Math.random() | 0).toString(16);
        if (item.road.length > 2) {
            init = item.road[0];
            for (k = 1; k < (item.road.length - 1); k++) {
                edges[j] = {from: init, to: item.road[k], color: color, length: item[1], value: 10, label: title, arrows: {to: {scaleFactor: 2}}};
                init = item.road[k];
                j++;
            }
        } else {
            edges[j] = {from: item.road[0], to: item.road[1], color: color, value: 10, title: title};
            j++;
        }
    });
    $("#heading-tbl-roads").html("Road more short");
}