function Crono() {
    control = '';
    hundredth = 0;
    seconds = 0;
    minutes = 0;
    hours = 0;
}
Crono.prototype.init = function () {
    this.control = setInterval(this.chronometer, 10);
}

Crono.prototype.stop = function () {
    clearInterval(this.control);
}

Crono.prototype.reinit = function () {
    clearInterval(this.control);
    this.hundredth = 0;
    this.seconds = 0;
    this.minutes = 0;
    this.hours = 0;
    Centesimas.innerHTML = ":00";
    Segundos.innerHTML = ":00";
    Minutos.innerHTML = ":00";
    Horas.innerHTML = "00";
}
Crono.prototype.chronometer = function () {
    if (this.hundredth < 99) {
        this.hundredth++;
        if (this.hundredth < 10) {
            this.hundredth = "0" + this.hundredth
        }
        Centesimas.innerHTML = ":" + this.hundredth;
    }
    if (this.hundredth == 99) {
        this.hundredth = -1;
    }
    if (this.hundredth == 0) {
        this.seconds++;
        if (this.seconds < 10) {
            this.seconds = "0" + this.seconds
        }
        Segundos.innerHTML = ":" + this.seconds;
    }
    if (this.seconds == 59) {
        this.seconds = -1;
    }
    if ((this.hundredth == 0) && (this.seconds == 0)) {
        this.minutes++;
        if (this.minutes < 10) {
            this.minutes = "0" + this.minutes
        }
        Minutos.innerHTML = ":" + this.minutes;
    }
    if (this.minutes == 59) {
        this.minutes = -1;
    }
    if ((this.hundredth == 0) && (this.seconds == 0) && (this.minutes == 0)) {
        this.hours++;
        if (this.hours < 10) {
            this.hours = "0" + this.hours
        }
        Horas.innerHTML = this.hours;
    }
};