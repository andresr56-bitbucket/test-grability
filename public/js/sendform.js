var frm = $('#start_limit');
frm.submit(function (e) {
    e.preventDefault();
    let alert = $(".alert-danger");
    let start = new Crono();
    if (typeof start.control != 'undefined')
        start.reinit();
    let eachJson = new FORMATdata();
    $("#matrixTable > thead, #matrixTable > tbody").html("");
    $("#roadsTable > tbody").html("");


    if (ACTIVEGRAPH){
        document.getElementById('bar').style.width = '0%';
        document.getElementById('text').innerHTML = '0%';
        $("#loadingBar").show();
    }

    $.ajax({
        type: frm.attr('method'),
        url: frm.attr('action'),
        data: frm.serialize(),
        beforeSend: function (request, settings) {
            if (alert.is(":visible"))
                alert.hide();
            start_time = new Date().getTime();
            start.init();
            $(".btn-danger").prop("disabled", true);
            $("#heading-tbl-matrix").html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Loading Dashboard of shopping centerss...');
            $("#heading-tbl-roads").html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Loading Road more short...');
            if (!$('#container_matrix').is(":visible")) $("#container_matrix").show();
            if (!$('#container_roads').is(":visible")) $("#container_roads").show();
        },
        success: function (data) {
            start.stop();
            $(".btn-danger").prop("disabled", false);
            if (typeof data.error === 'undefined') {
                eachJson.buildData(data);
            } else {
                alert.html(data.error);
                if (!alert.is(":visible"))
                    alert.show();
            }
        },
        error: function (data) {
            console.log('An error occurred.');
            console.log(data);
        },
    });
});
