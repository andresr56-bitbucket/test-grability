var ACTIVEGRAPH = false;

$('#myStateButton').on('click', function () {
    ACTIVEGRAPH = (ACTIVEGRAPH) ? false : true;
    if (!ACTIVEGRAPH)
        $("#container_roads_graph").hide();
});

function buildGraph(n, e) {
    var container = document.getElementById('mynetworkdos');
    var data = {
        nodes: n,
        edges: e
    };
    var options = {
        nodes: {
            shape: 'dot',
            size: 16
        },
        layout: {
            randomSeed: 34
        },
        physics: {
            forceAtlas2Based: {
                gravitationalConstant: -26,
                centralGravity: 0.005,
                springLength: 230,
                springConstant: 0.18
            },
            maxVelocity: 146,
            solver: 'forceAtlas2Based',
            timestep: 0.35,
            stabilization: {
                enabled: true,
                iterations: 2000,
                updateInterval: 25
            }
        }
    };
    var network = new vis.Network(container, data, options);
    network.on("stabilizationProgress", function (params) {
        var maxWidth = 1048;
        var minWidth = 0;
        var widthFactor = params.iterations / params.total;
        var width = Math.max(minWidth, maxWidth * widthFactor);
        document.getElementById('mynetworkdos').style.width = 100 + '%';
        document.getElementById('mynetworkdos').style.height = 100 + 'px';
        $("#container_roads_graph").show();
        document.getElementById('bar').style.width = width + 'px';
        document.getElementById('text').innerHTML = Math.round(widthFactor * 100) + '%';
    });
    network.once("stabilizationIterationsDone", function () {
        document.getElementById('mynetworkdos').style.height = 400 + 'px';
        document.getElementById('text').innerHTML = '100%';
        document.getElementById('bar').style.width = '1048px';
        $("#loadingBar").hide(1000);
    });
}