<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ValuesMatrix extends Model
{
    protected $table = 'matrices';
    protected $fillable = ['meta_data'];
    protected $guarded = ['id'];
    protected $connection = 'pgsql';
    
    public function saveMatrix($value) {
        try {
            
            $this->meta_data = json_encode($value);
            $this->save();
            $id = $this->id;
            return $id;
        } catch (Exception $exc) {
            \Log::info('Error creating saveMatrix: ' . $exc);
            return FALSE;
        }
    }
}
