<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Traits\Matrix;
use App\Traits\Way;
use Illuminate\Support\Facades\Redis;

class HomeController extends Controller {

    use Matrix;
    use Way;

    /* N shopping centers
     * M roads
     * K types of fish
     * 
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        
        return view('home');
    }

    public function makeMatrix(Request $request) {
        try {
            if (!$request->isMethod('post'))
                return redirect('home');

            $validator = Validator::make($request->all(), [
                        'shoppings' => 'required|integer|min:4|max:10000'
            ]);
            if ($validator->fails())
                return ['error' => $validator->errors()->all()];

            $limit = $request->shoppings;
            
            $this->buildMatrix($limit);
            
            $roadBigCat = rand(1, ($limit - 2));
            $roadLittleCat = rand(1, ($limit - 2));
            while ($roadBigCat === $roadLittleCat) {
                $roadLittleCat = rand(1, ($limit - 2));
            }
            $graphBigCat = $this->buildGraph($roadLittleCat);
            $graphLittleCat = $this->buildGraph($roadBigCat);
            //        foreach ($graph as $i => $gh){
            //            $citiInit = $gh['shop_center_init']['name'];
            //            $citiFin = $gh['shop_center_fin']['name'];
            //            $path[$i] = $this->shortWay($graph, $citiInit, $citiFin);
            //        }
            $path[0] = $this->shortWay($graphLittleCat, 'C0', 'C' . ($limit - 1));
            $path[1] = $this->shortWay($graphBigCat, 'C0', 'C' . ($limit - 1));
            return ['roads' => array('pathLittleCat' => array('name' => 'LittleCat', 'road' => $path[0]), 'pathBigCat' => array('name' => 'BigCat', 'road' => $path[1])), 'matrix' => $this->matrix, 'shoppings' => $this->shoppings];
        } catch (Exception $ex) {
            \Log::info('Error makeMatrix: ' . $e);
            return \Response::json(['solution cat fish' => $e], 500);
            
        }
    }

}
