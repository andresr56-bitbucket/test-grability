<?php

namespace App\Traits;

use Illuminate\Support\Facades\Redis;
use App\ValuesMatrix;

/**
 *
 * @author andresr56@gmail.com
 * */
trait Matrix {

    public $matrix;
    private $limit;
    public $shoppings;
    private $graph;
    public $id;
    protected $valuesMatrix;

    private function initializeMatrix() {
        $this->matrix = [[]];
        $this->valuesMatrix = new ValuesMatrix();
    }

    public function buildMatrix($limit) {

        $this->initializeMatrix();
        $this->limit = $limit;
        settype($this->limit, 'integer');
        $mtx = [[]];
        #llenar matrix con diagonal inversa en cero y valores rand entre 1 y N
        for ($i = 0; $i < $this->limit; $i++) {
            for ($j = 0; $j < $this->limit; $j++) {
                $mtx[$i][$j] = ($i === $j) ? 0 : rand(1, (2 * $limit));
            }
        }
        foreach ($mtx as $key => $value) {
            foreach ($value as $k => $v) {
                if ($key > 0) {
                    $mtx[$key][$k] = $mtx[$k][$key];
                }
            }
        }
        $this->id = $this->valuesMatrix->saveMatrix($mtx);
    }

    public function buildGraph($roadInit) {
        $this->getShoppingCenter();
        $this->getTypesFish();
        try {
            if ($this->id !== FALSE){
                $i = 0;
                $graph = array();
                $this->matrix = json_decode($this->valuesMatrix->find($this->id)->meta_data);
                foreach ($this->matrix as $key => $value) {
                    if ($key > 0) {
                        foreach ($value as $k => $v) {
                            if (Redis::hget("shopping_center:$key", 'name') !== Redis::hget("shopping_center:$k", 'name')) {
                                $graph[$i] = array('shop_center_init' => Redis::hget("shopping_center:$key", 'name'), 'shop_center_fin' => Redis::hget("shopping_center:$k", 'name'), 'time' => $this->matrix[$key][$k]);
                                $i++;
                            }
                        }
                    } else {
                        $graph[$i] = array('shop_center_init' => Redis::hget("shopping_center:$key", 'name'), 'shop_center_fin' => Redis::hget("shopping_center:$roadInit", 'name'), 'time' => $this->matrix[$key][$roadInit]);
                        $i++;
                    }
                }
                return $graph;
            }
        } catch (Exception $exc) {
            \Log::info('Error creating buildGraph: ' . $exc);
        }
    }

    private function getTypesFish() {
        try {
            $limitFish = rand(1, 10);
            for ($i = 0; $i < $this->limit; $i++) {
                for ($t = 1; $t <= $limitFish; $t++) {
                    if ($i > 0) {
                        $type = $t;
                        $amount = rand(0, $limitFish);
                        $types = array('type' => $i, 'amount' => $t);
                        Redis::hmset("shopping_center:$i", 'fish_type', $i, 'fish_amount', $t);
                        array_push($this->shoppings[$i]['fish_inventory'], $types);
                    }
                }
            }
        } catch (Exception $exc) {
            \Log::info('Error creating getTypesFish: ' . $exc);
        }
    }

    private function getShoppingCenter() {
        try {
            for ($i = 0; $i < $this->limit; $i++) {
                Redis::hmset("shopping_center:$i", 'name', "C$i");
                $this->shoppings[$i] = array('name' => "C$i", 'fish_inventory' => array());
            }
        } catch (Exception $exc) {
            \Log::info('Error creating getShoppingCenter: ' . $exc);
        }
    }

}
