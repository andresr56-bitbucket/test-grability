<?php

namespace App\Traits;

/**
 * Description of Way
 *
 * @author andresr56@gmail.com
 */
trait Way {

    private $negthbours;
    private $way;
    private $init;
    private $end;
    private $dist;
    private $previus;

    private function initializeWay() {
        $this->negthbours = array();
        $this->way = array();
        $this->dist = array();
        $this->previus = array();
    }

    public function shortWay($graph, $init, $end) {
        $this->initializeWay();
        $this->graph = $graph;
        $this->init = $init;
        $this->end = $end;
        return $this->findWay();
    }

    private function completeApex() {
        $vertices = array();
        foreach ($this->graph as $edge) {
                #dd($edge);exit();
                array_push($vertices, $edge['shop_center_init'], $edge['shop_center_fin']); //llenar array vertices con origen y destino
                $this->negthbours[$edge['shop_center_init']][] = array('endEdge' => $edge['shop_center_fin'], 'cost' => $edge['time']); //llenar ciudad origen con las ciudades destinos y sus pesos en array
        }
        return array_unique($vertices);
    }

    private function findWay() {
        $path = array();

        $vertices = array_unique($this->completeApex()); //unificar array para hallar vertice con todas las ciudades origen y destino
        foreach ($vertices as $vertex) {
            $dist[$vertex] = INF;
            $previus[$vertex] = NULL;
        }
        /* $dist es el array de ciudades con la distacina definida en INF excep el origen que es cero* */
        $dist[$this->init] = 0; //dentro del array dist se definie en cero la ciudad origen las demás ciudades se setearon como INF
        $i = 0;
        while (count($vertices) > 0) {
            $min = INF;
            foreach ($vertices as $ciudad) {
                if ($dist[$ciudad] < $min) {
                    $min = $dist[$ciudad];
                    $u = $ciudad;
                }
            }

            $vertices = array_diff($vertices, array($u));
            if ($dist[$u] === INF || $u === $this->end) {
                array_unshift($path, $dist[$u]);
                break;
            }
            if (isset($this->negthbours[$u])) {
                foreach ($this->negthbours[$u] as $arr) {
                    $alt = $dist[$u] + $arr['cost'];
                    if ($alt < $dist[$arr['endEdge']]) {
                        $cost[$arr['endEdge']] = $dist[$arr['endEdge']];
                        $dist[$arr['endEdge']] = $alt;
                        $previus[$arr['endEdge']] = $u;
                    }
                }
            }
        }
        $u = $this->end;
        while (isset($previus[$u])) {
            array_unshift($path, $u);
            $u = $previus[$u];
        }
        array_unshift($path, $u);

        return $path;
    }
}
