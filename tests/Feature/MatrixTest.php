<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class MatrixTest extends TestCase
{
        
    /**
     * A basic test example.
     * @test
     * @group matrixJSON
     * @return void
     */
    public function getJsonMatrix()
    {
        $response = $this->json('POST', 'matrix', ['shoppings' => 5]);
        $response->assertStatus(200)
                ->assertJson([
                    'shoppings' => TRUE,
                    'roads' => TRUE,
                    'matrix' => TRUE,
                ])
                ->assertJsonFragment([
                    'name' => 'BigCat',
                    'name' => 'LittleCat',
                    'name' => 'C0',
                ]);
    }
}
