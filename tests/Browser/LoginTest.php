<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    #use DatabaseMigrations;
    
    protected $user;
    
    public function setUp() {
        parent::setUp();
        $this->user = factory(User::class)->create(['email' => 'yourusername@last.com']);
    }
    
    /*
     * @test
     * @return void
     */
    public function loginLoadMatrix() {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                    ->assertSee('MATRIX')
                    ->clickLink('Login')
                    ->assertPathIs('/login')
                    ->type('email', $this->user->email)
                    ->type('password','yourpassword')
                    ->press('Login')
                    ->assertPathIs('/home')
                    ->type('shoppings', 5)
                    ->press('Send')
                    ->assertSee('Road more short')
                    ->assertSee('LittleCat')
                    ->assertSee('Dashboard of shopping centers')
                    ->assertSee('C4')
                    ->type('shoppings', 5)
                    ->check('#myStateButton')
                    ->press('Send')
                    ->assertSee('Graph Road more short');
        });
    }
    
    
}
