<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ExampleTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     * @test
     *
     * @return void
     */
    public function basicExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/matrix_laravel/matrix/public/')
                    ->assertSee('MATRIX');
        });
    }
}
